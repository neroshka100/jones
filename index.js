const puppeteer= require ( 'puppeteer' );

describe("Jones automation- Exercise", () => {
    it('Should comlete the task', async function() {
        const browser = await puppeteer.launch ( {headless : false} ) ;
        const page = await browser .newPage ( );
        await page.goto ( "http://contractorsinsurancereview.com/ExampleForm/" );
        await page.type ( ' # name ' , ' Neriya ' ) ;
        await page.type ( ' # email ' , ' neroshka100@ gmail.com ' );
        await page.type ( ' # phone ' , ' 0587005060 ' );
        await page.type ( ' # company ' , ' jones ' );
        await page.select ( ' # employees ' , '51 -500 ' );
        await page.click ( ' . primary button ' );
        await page.waitForSelector ( ' h1 ' ) ;
        await page.screenshot ( );
    });
});
